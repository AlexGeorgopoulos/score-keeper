const numOfPlayersSelect = document.getElementById('number-of-players');
const playToSelect = document.getElementById("playto");
const resetButton = document.getElementById("reset");
const displaySection = document.getElementById('display-section');
const buttonsSection = document.getElementById('buttons-section');
const progressSection = document.getElementById('progress-bar-section');
const winnerText = document.getElementById("winner-text");

let numberOfPlayers = Number(numOfPlayersSelect.value);
let winningScore = Number(playToSelect.value);
let isGameOver = false;
let players = []

numOfPlayersSelect.addEventListener('change', function () {
    numberOfPlayers = Number(this.value);
    reset();
});

playToSelect.addEventListener('change', function () {
    winningScore = Number(this.value);
    reset();
});

resetButton.addEventListener('click', function () {
    reset();
});

class Player {
    constructor(id) {
        this.id = id;
        this.score = 0;
        this.display = document.querySelector(`#p${id}Display`);
        this.button = document.querySelector(`#p${id}Button`);
        this.progressBar = document.querySelector(`#p${id}ProgressBar`);
    }

    updateScore() {
        this.score += 1;

        this.#updateProgressBar();

        this.display.innerHTML = `Player ${this.id}: ${this.score}`;

        if (this.score === winningScore) {
            finishGame(this.id);
        }
    }

    #updateProgressBar() {
        const step = 100 / winningScore;
        this.progressBar.value = this.score * step;
    }
}

function playerButtonClickHandler(event) {
    targetId = event.target.getAttribute('data-id');

    playerInstace = players.find(player => player.id === Number(targetId));

    playerInstace.updateScore()
}

function startGame() {
    for (let i = 1; i <= numberOfPlayers; i++) {
        const title = document.createElement("h4");
        title.setAttribute("id", `p${i}Display`)
        title.innerHTML = `Player ${i}: 0`;
        displaySection.appendChild(title);

        const button = document.createElement("button");
        button.setAttribute("id", `p${i}Button`);
        button.setAttribute("data-id", i);
        button.setAttribute("class", "button player-button is-primary is-rounded");
        button.innerHTML = `+1 to Player ${i}`;
        buttonsSection.appendChild(button);

        const progressBar = document.createElement("progress");
        progressBar.setAttribute("id", `p${i}ProgressBar`)
        progressBar.setAttribute("class", 'progress is-success');
        progressBar.setAttribute("value", 0);
        progressBar.setAttribute("max", 100);
        progressSection.appendChild(progressBar);

        players.push(new Player(i));
    }

    document.querySelectorAll('.player-button').forEach(button => {
        button.addEventListener('click', playerButtonClickHandler);
    });
}

function finishGame(winnerId) {
    isGameOver = true;

    winnerText.innerHTML = `<span class='has-text-primary'> Player${winnerId} wins!!! </span>`;
    
    document.querySelectorAll('.player-button').forEach(button => {
        button.disabled = true;
        button.removeEventListener('click', playerButtonClickHandler);
    });
}

function reset() {
    isGameOver = false;

    players = [];

    while (displaySection.firstChild) {
        displaySection.removeChild(displaySection.firstChild);
    }

    while (buttonsSection.firstChild) {
        buttonsSection.removeChild(buttonsSection.firstChild);
    }

    while (progressSection.firstChild) {
        progressSection.removeChild(progressSection.firstChild);
    }

    winnerText.innerHTML = "";

    this.startGame();
}

startGame();